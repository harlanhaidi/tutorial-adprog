package applicant;

import static applicant.Applicant.evaluate;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;
import org.junit.Test;


public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!

    @Test
    public void testApplicant() {
        Applicant applicant = new Applicant();

        Predicate<Applicant> creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;

        Predicate<Applicant> employmentCheck =
                theApplicant -> theApplicant.getEmploymentYears() > 0;

        Predicate<Applicant> crimeCheck = theApplicant -> ! theApplicant.hasCriminalRecord();

        assertEquals(true, evaluate(applicant, creditCheck));
        assertEquals(true, evaluate(applicant, creditCheck.and(employmentCheck)));
        assertEquals(false, evaluate(applicant, crimeCheck.and(employmentCheck)));
        assertEquals(true, evaluate(applicant, creditCheck.and(creditCheck).and(employmentCheck)));
    }
}
