import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    @Test
    public void testGroupByScores() {
        Map<String, Integer> scores = new HashMap<>();

        scores.put("Alice", 15);
        scores.put("Bob", 10);
        scores.put("Charlie", 10);

        List<String> listA = new ArrayList<>();
        listA.add("Alice");
        List<String> listB = new ArrayList<>();
        listB.add("Bob");
        listB.add("Charlie");

        Map<Integer, List<String>> hasil = new HashMap<>();
        hasil.put(15, listA);
        hasil.put(10, listB);

        assertEquals(hasil, ScoreGrouping.groupByScores(scores));
    }

}