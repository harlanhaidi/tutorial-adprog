package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class DecaoratorMain {
    public static void main (String[] arg) {
        Food roti = new CrustySandwich();
        roti = new BeefMeat(roti);
        roti = new Cucumber(roti);
        System.out.println(roti.getDescription() + ". " + roti.cost());

        Food roti2 = new NoCrustSandwich();
        roti2 = new Cheese(roti2);
        roti2 = new Lettuce(roti2);
        System.out.println(roti2.getDescription() + ". " + roti2.cost());

        Food roti3 = new ThickBunBurger();
        roti3 = new ChiliSauce(roti3);
        roti3 = new Tomato(roti3);
        System.out.println(roti3.getDescription() + ". " + roti3.cost());

        Food roti4 = new ThinBunBurger();
        roti4 = new ChickenMeat(roti4);
        roti4 = new TomatoSauce(roti4);
        System.out.println(roti3.getDescription() + ". " + roti3.cost());

    }
}
