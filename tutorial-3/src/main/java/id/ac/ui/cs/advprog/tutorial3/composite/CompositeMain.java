package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;

public class CompositeMain {
    public static void main (String[] args) {
        Company company = new Company();

        Ceo luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Cto zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        BackendProgrammer franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        FrontendProgrammer nami = new FrontendProgrammer("Nami",66000.00);
        company.addEmployee(nami);

        UiUxDesigner sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        NetworkExpert brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        SecurityExpert chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);

        List<Employees> list = company.getAllEmployees();
        double totalSalary = company.getNetSalaries();

        System.out.println("Jumlah Employee : " + list.size());
        System.out.println("Jumlah Salary     : " + totalSalary);

        for (Employees employee : list) {
            System.out.println("Nama : " + employee.getName() + ", Role : " + employee.getRole() + ", Salary : " + employee.getSalary());
        }
    }
}
