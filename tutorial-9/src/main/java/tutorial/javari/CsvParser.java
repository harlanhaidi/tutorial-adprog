package tutorial.javari;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class CsvParser {
    ArrayList<Animal> list = new ArrayList<Animal>();

    public ArrayList<Animal> parseCsv() throws IOException {
        String line = "";
        String cvsSplitBy = ",";


        try (BufferedReader br = new BufferedReader(
                new FileReader("src/main/java/tutorial/javari/records.csv"))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);

                int id = Integer.parseInt(data[0]);
                Gender gender = Gender.parseGender(data[3]);
                double length = Double.parseDouble(data[4]);
                double height = Double.parseDouble(data[5]);
                Condition condition = Condition.parseCondition(data[6]);

                list.add(new Animal(id, data[1], data[2], gender, length, height, condition));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

}
