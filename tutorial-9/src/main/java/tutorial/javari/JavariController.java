package tutorial.javari;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;


@RestController
public class JavariController {
    // TODO Implement me!
    CsvParser csvParser = new CsvParser();
    ArrayList<Animal> list;

    {
        try {
            list = csvParser.parseCsv();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/javari", method = GET)
    public Object javaria() {

        if (list.isEmpty()) {
            return "list kosong";
        }

        return list;
    }

    @RequestMapping(value = "/javari/{ID:[0-9]+}", method = GET)
    public Object javariaId(@PathVariable("ID") int id) {

        if (list.isEmpty()) {
            return "list kosong";
        }

        for (Animal a : list) {
            if (a.getId() == id) {
                return a;
            }
        }
        return "binatang tidak ada";
    }

    @RequestMapping(value = "/javari/{ID:[0-9]+}", method = DELETE)
    public Object javariaIdDelete(@PathVariable("ID") int id) {

        if (list.isEmpty()) {
            return "list kosong";
        }

        for (Animal a : list) {
            if (a.getId() == id) {
                Animal dihapus = a;
                list.remove(a);
                return dihapus;
            }
        }

        return "binatang tidak ada";
    }

    @RequestMapping(value = "/javari", method = POST)
    public void javariaPost(@RequestBody String json) throws IOException {
        HashMap hasil = new ObjectMapper().readValue(json, HashMap.class);

        Integer id = new Integer(String.valueOf(hasil.get("id")));
        Double length = new Double(String.valueOf(hasil.get("length")));
        Double weight = new Double(String.valueOf(hasil.get("length")));

        Animal animal = new Animal(id, hasil.get("type").toString(), hasil.get("name").toString(),
                Gender.parseGender(hasil.get("gender").toString()), length, weight,
                Condition.parseCondition(hasil.get("condition").toString()));

        list.add(animal);

    }
}
