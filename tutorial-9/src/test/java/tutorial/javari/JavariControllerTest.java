package tutorial.javari;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    // TODO Implement me! (additional task)

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void javariGet() throws Exception {
        this.mockMvc.perform(get("/javari")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void javariGetId() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Animal animal = new Animal(65, "Hamster", "hamtaro", Gender.parseGender("male"),
                5,0.25, Condition.parseCondition("healthy"));

        this.mockMvc.perform(post("/javari")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(animal)))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/javari/65")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("65"));
    }

    @Test
    public void javariGetIdTidakAda() throws Exception {
        this.mockMvc.perform(get("/javari/89")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("binatang tidak ada")));
    }

    @Test
    public void javariGetIdDelete() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Animal animal = new Animal(67, "Hamster", "hamtaro", Gender.parseGender("male"),
                5,0.25, Condition.parseCondition("healthy"));

        this.mockMvc.perform(post("/javari")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(animal)))
                .andExpect(status().isOk());

        this.mockMvc.perform(delete("/javari/67")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("67"));
    }

    @Test
    public void javariDeleteIdTidakAda() throws Exception {
        this.mockMvc.perform(get("/javari/67")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("binatang tidak ada")));
    }

    @Test
    public void javariPost() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        Animal animal = new Animal(1, "Hamster", "hamtaro", Gender.parseGender("male"),
                5,0.25, Condition.parseCondition("healthy"));

        this.mockMvc.perform(post("/javari")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(animal)))
                .andExpect(status().isOk());
    }

}